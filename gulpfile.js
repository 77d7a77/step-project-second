const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const concat = require("gulp-concat");
const autoprefixer = require("gulp-autoprefixer");
const uglify = require('gulp-uglify');
const deleteFile = require('delete');
const sass = require("gulp-sass")(require('sass'));
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
gulp.task("minify",()=>{
    return gulp.src("src/html/*.html")
    .pipe(htmlmin({collapseWhitespace:true}))
    .pipe(gulp.dest("dist"));
})
gulp.task("build-css",function(){
    return gulp.src("src/styles/style.scss")
    .pipe(autoprefixer({cascade: false }))
    .pipe(sass().on('error', sass.logError))
    // .pipe(concat("styles.css"))
    .pipe(cssmin())
    .pipe(rename({suffix:".min"}))
    .pipe(gulp.dest("dist"));
})
gulp.task("build-js",function(){
    return gulp.src("src/js/*.js")
    .pipe(uglify())
    .pipe(gulp.dest("dist"));
})
gulp.task("buildImg", function () {
    return gulp.src("src/img/**/*.png").pipe(gulp.dest("dist/img"));
  });
gulp.task("deleteFiles",function(){
    return deleteFile("dist")
})
gulp.task("build",gulp.series("deleteFiles",gulp.parallel("buildImg","minify","build-js","build-css")))
gulp.watch(
    ["src/styles/**/**/*.scss", "src/html/**/**/*.html", "src/js/**/**/*.js"],
    gulp.series("build")
  );